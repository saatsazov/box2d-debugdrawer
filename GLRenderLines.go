package debugDraw

import (
	"fmt"
	"strings"

	"github.com/go-gl/gl/v4.1-core/gl"

	"github.com/saatsazov/box2d"
)

const GLRenderLines_e_maxVertices = 2 * 512

type GLRenderLines struct {
	m_vertices [GLRenderLines_e_maxVertices * 2]float32
	m_colors   [GLRenderLines_e_maxVertices * 4]float32

	m_count int32

	m_vaoId             uint32
	m_vboIds            [2]uint32
	m_programId         uint32
	m_projectionUniform uint32
	m_vertexAttribute   uint32
	m_colorAttribute    uint32

	camera Camera
}

func (r *GLRenderLines) Create() {
	r.camera.Create()

	vs := `
#version 330
uniform mat4 projectionMatrix;
layout(location = 0) in vec2 v_position;
layout(location = 1) in vec4 v_color;
out vec4 f_color;
void main(void)
{
	f_color = v_color;
	gl_Position = projectionMatrix * vec4(v_position, 0.0f, 1.0f);
}`
	fs := `
#version 330
in vec4 f_color;
out vec4 color;
void main(void)
{
	color = f_color;
}`

	r.m_programId = sCreateShaderProgram(vs, fs)
	r.m_projectionUniform = uint32(gl.GetUniformLocation(r.m_programId, gl.Str("projectionMatrix\x00")))
	r.m_vertexAttribute = 0
	r.m_colorAttribute = 1

	gl.GenVertexArrays(1, &r.m_vaoId)
	gl.GenBuffers(2, &r.m_vboIds[0])

	gl.BindVertexArray(r.m_vaoId)
	gl.EnableVertexAttribArray(r.m_vertexAttribute)
	gl.EnableVertexAttribArray(r.m_colorAttribute)

	gl.BindBuffer(gl.ARRAY_BUFFER, r.m_vboIds[0])
	gl.VertexAttribPointerWithOffset(r.m_vertexAttribute, 2, gl.FLOAT, false, 0, 0)
	gl.BufferData(gl.ARRAY_BUFFER, len(r.m_vertices)*4, gl.Ptr(&r.m_vertices[0]), gl.DYNAMIC_DRAW)

	gl.BindBuffer(gl.ARRAY_BUFFER, r.m_vboIds[1])
	gl.VertexAttribPointerWithOffset(r.m_colorAttribute, 4, gl.FLOAT, false, 0, 0)
	gl.BufferData(gl.ARRAY_BUFFER, len(r.m_colors)*4, gl.Ptr(&r.m_colors[0]), gl.DYNAMIC_DRAW)

	sCheckGLError()

	gl.BindBuffer(gl.ARRAY_BUFFER, 0)
	gl.BindVertexArray(0)
	r.m_count = 0

}

func sCheckGLError() {
	errCode := gl.GetError()
	if errCode != gl.NO_ERROR {
		fmt.Printf("OpenGL error = %d\n", errCode)
		panic("error OPENGL")
	}
}

func sCreateShaderFromString(source string, shaderType uint32) uint32 {
	res := gl.CreateShader(shaderType)
	csources, free := gl.Strs(source + "\x00")
	gl.ShaderSource(res, 1, csources, nil)
	free()
	gl.CompileShader(res)
	var status int32
	gl.GetShaderiv(res, gl.COMPILE_STATUS, &status)
	if status == gl.FALSE {
		var logLength int32
		gl.GetShaderiv(res, gl.INFO_LOG_LENGTH, &logLength)

		log := strings.Repeat("\x00", int(logLength+1))
		gl.GetShaderInfoLog(res, logLength, nil, gl.Str(log))

		fmt.Println(log)
		return 0
	}

	return res
}

func sCreateShaderProgram(vs string, fs string) uint32 {
	vsId := sCreateShaderFromString(vs, gl.VERTEX_SHADER)
	fsId := sCreateShaderFromString(fs, gl.FRAGMENT_SHADER)

	if vsId == 0 || fsId == 0 {
		panic("Can't create shader")
	}

	programId := gl.CreateProgram()
	gl.AttachShader(programId, vsId)
	gl.AttachShader(programId, fsId)
	gl.BindFragDataLocation(programId, 0, gl.Str("color\x00"))
	gl.LinkProgram(programId)

	var status int32
	gl.GetProgramiv(programId, gl.LINK_STATUS, &status)
	if status == gl.FALSE {
		var logLength int32
		gl.GetProgramiv(programId, gl.INFO_LOG_LENGTH, &logLength)

		log := strings.Repeat("\x00", int(logLength+1))
		gl.GetProgramInfoLog(programId, logLength, nil, gl.Str(log))
		fmt.Printf("failed to link program: %v", log)
		return 0
	}

	gl.DeleteShader(vsId)
	gl.DeleteShader(fsId)

	return programId
}

func (r *GLRenderLines) Destroy() {

}

func (r *GLRenderLines) Vertex(v box2d.B2Vec2, c box2d.B2Color) {
	if r.m_count == GLRenderLines_e_maxVertices {
		r.Flush()
	}

	r.m_vertices[r.m_count*2] = float32(v.X)
	r.m_vertices[(r.m_count*2)+1] = float32(v.Y)

	r.m_colors[r.m_count*4] = float32(c.R)
	r.m_colors[(r.m_count*4)+1] = float32(c.G)
	r.m_colors[(r.m_count*4)+2] = float32(c.B)
	r.m_colors[(r.m_count*4)+3] = float32(c.A)
	r.m_count++
}

func (r *GLRenderLines) Flush() {
	if r.m_count == 0 {
		return
	}

	gl.UseProgram(r.m_programId)

	proj := r.camera.BuildProjectionMatrix(0.1)
	gl.UniformMatrix4fv(int32(r.m_projectionUniform), 1, false, &proj[0])

	gl.BindVertexArray(r.m_vaoId)

	gl.BindBuffer(gl.ARRAY_BUFFER, r.m_vboIds[0])
	gl.BufferSubData(gl.ARRAY_BUFFER, 0, int(r.m_count)*2*4, gl.Ptr(&r.m_vertices[0]))

	gl.BindBuffer(gl.ARRAY_BUFFER, r.m_vboIds[1])
	gl.BufferSubData(gl.ARRAY_BUFFER, 0, int(r.m_count)*4*4, gl.Ptr(&r.m_colors[0]))

	gl.DrawArrays(gl.LINES, 0, r.m_count)

	sCheckGLError()

	gl.BindBuffer(gl.ARRAY_BUFFER, 0)
	gl.BindVertexArray(0)
	gl.UseProgram(0)

	r.m_count = 0
}
