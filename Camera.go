package debugDraw

import "github.com/saatsazov/box2d"

type Camera struct {
	m_center box2d.B2Vec2
	m_zoom   float32
	m_width  int32
	m_height int32
}

func (c *Camera) Create() {
	c.m_center = box2d.MakeB2Vec2(0, 0)
	c.m_zoom = 1
	c.m_width = 1280
	c.m_height = 800
}

// Convert from world coordinates to normalized device coordinates.
// http://www.songho.ca/opengl/gl_projectionmatrix.html
func (c *Camera) BuildProjectionMatrix(zBias float32) (m []float32) {
	m = make([]float32, 16)
	w := float32(c.m_width)
	h := float32(c.m_height)
	ratio := w / h
	extents := box2d.MakeB2Vec2(float64(ratio*25.0), 25.0)
	extents.OperatorScalarMulInplace(float64(c.m_zoom))

	lower := box2d.B2Vec2Sub(c.m_center, extents)
	upper := box2d.B2Vec2Add(c.m_center, extents)

	m[0] = float32(2.0 / (upper.X - lower.X))
	m[1] = 0.0
	m[2] = 0.0
	m[3] = 0.0

	m[4] = 0.0
	m[5] = float32(2.0 / (upper.Y - lower.Y))
	m[6] = 0.0
	m[7] = 0.0

	m[8] = 0.0
	m[9] = 0.0
	m[10] = 1.0
	m[11] = 0.0

	m[12] = float32(-(upper.X + lower.X) / (upper.X - lower.Y))
	m[13] = float32(-(upper.Y + lower.Y) / (upper.Y - lower.Y))
	m[14] = zBias
	m[15] = 1.0
	return m
}
