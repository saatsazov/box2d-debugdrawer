package debugDraw

import (
	"math"

	"github.com/saatsazov/box2d"
)

type DebugDraw struct {
	lines GLRenderLines
}

func (d *DebugDraw) Create() {
	d.lines.Create()
}

func (d *DebugDraw) Flush() {
	d.lines.Flush()
}

func (draw *DebugDraw) GetFlags() uint {
	return box2d.B2Draw_e_shapeBit

}

func (d *DebugDraw) DrawCircle(center box2d.B2Vec2, radius float64, axis box2d.B2Vec2, color box2d.B2Color) {
	const k_segments = 16.0
	const k_increment = 2.0 * math.Pi / k_segments
	sinInc := math.Sin(k_increment)
	cosInc := math.Cos(k_increment)
	r1 := box2d.MakeB2Vec2(1.0, 0.0)
	v1 := box2d.B2Vec2Add(center, box2d.B2Vec2MulScalar(radius, r1))
	for i := 0; i < k_segments; i++ {
		// Perform rotation to avoid additional trigonometry.
		var r2 box2d.B2Vec2
		r2.X = cosInc*r1.X - sinInc*r1.Y
		r2.Y = sinInc*r1.X + cosInc*r1.Y
		v2 := box2d.B2Vec2Add(center, box2d.B2Vec2MulScalar(radius, r2))
		d.lines.Vertex(v1, color)
		d.lines.Vertex(v2, color)
		// fmt.Printf("line: %v %v -> ", v1, color)
		// fmt.Printf(" %v %v\n", v2, color)
		r1 = r2
		v1 = v2
	}
}

func (d *DebugDraw) DrawSolidCircle(center box2d.B2Vec2, radius float64, axis box2d.B2Vec2, color box2d.B2Color) {
	const k_segments = 16.0
	const k_increment = 2.0 * math.Pi / k_segments
	sinInc := math.Sin(k_increment)
	cosInc := math.Cos(k_increment)
	// v0 := center
	r1 := box2d.MakeB2Vec2(cosInc, sinInc)
	v1 := box2d.B2Vec2Add(center, box2d.B2Vec2MulScalar(radius, r1))
	// fillColor := MakeB2ColorAlpha(0.5*color.R, 0.5*color.G, 0.5*color.B, 0.5)
	// for (int32 i = 0; i < k_segments; ++i)
	// {
	// 	// Perform rotation to avoid additional trigonometry.
	// 	b2Vec2 r2;
	// 	r2.x = cosInc * r1.x - sinInc * r1.y;
	// 	r2.y = sinInc * r1.x + cosInc * r1.y;
	// 	b2Vec2 v2 = center + radius * r2;
	// 	m_triangles->Vertex(v0, fillColor);
	// 	m_triangles->Vertex(v1, fillColor);
	// 	m_triangles->Vertex(v2, fillColor);
	// 	r1 = r2;
	// 	v1 = v2;
	// }

	r1.Set(1, 0)
	v1 = box2d.B2Vec2Add(center, box2d.B2Vec2MulScalar(radius, r1))
	for i := 0; i < k_segments; i++ {
		var r2 box2d.B2Vec2
		r2.X = cosInc*r1.X - sinInc*r1.Y
		r2.Y = sinInc*r1.X + cosInc*r1.Y
		v2 := box2d.B2Vec2Add(center, box2d.B2Vec2MulScalar(radius, r2))
		d.lines.Vertex(v1, color)
		d.lines.Vertex(v2, color)
		r1 = r2
		v1 = v2
	}

	p := box2d.B2Vec2Add(center, box2d.B2Vec2MulScalar(radius, axis))
	d.lines.Vertex(center, color)
	d.lines.Vertex(p, color)
}

func (draw *DebugDraw) DrawSolidPolygon(vertices []box2d.B2Vec2, vertexCount int, color box2d.B2Color) {
	// fillColor := MakeB2ColorAlpha(0.5*color.R, 0.5*color.G, 0.5*color.B, 0.5)
	for i := 0; i < vertexCount; i++ {
		// m_triangles->Vertex(vertices[0], fillColor);
		// m_triangles->Vertex(vertices[i], fillColor);
		// m_triangles->Vertex(vertices[i + 1], fillColor);
	}

	p1 := vertices[vertexCount-1]
	for i := 0; i < vertexCount; i++ {
		p2 := vertices[i]
		draw.lines.Vertex(p1, color)
		draw.lines.Vertex(p2, color)
		p1 = p2
	}
}

func (d *DebugDraw) DrawSegment(v1, v2 box2d.B2Vec2, color box2d.B2Color) {
	d.lines.Vertex(v1, color)
	d.lines.Vertex(v2, color)
}
