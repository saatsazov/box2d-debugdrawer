module github.com/saatsazov/box2d-debugdrawer

go 1.16

require (
	github.com/go-gl/gl v0.0.0-20210426225639-a3bfa832c8aa
	github.com/go-gl/glfw/v3.3/glfw v0.0.0-20210410170116-ea3d685f79fb
	github.com/saatsazov/box2d v1.0.3-0.20210524174930-2712c2446a92
)
