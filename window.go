package debugDraw

import (
	"log"
	"runtime"

	"github.com/go-gl/gl/v4.1-core/gl"
	"github.com/go-gl/glfw/v3.3/glfw"
	"github.com/saatsazov/box2d"
)

func init() {
	runtime.LockOSThread()
}

const windowWidth = 800
const windowHeight = 600

var closing bool = false

func InitDebugDraw(world *box2d.B2World, closeChanel <-chan bool) {
	debugDraw := DebugDraw{}
	world.G_debugDraw = &debugDraw
	closing = false

	go func() {
		<-closeChanel
		closing = true
	}()
	createWindow(world)
}

func createWindow(world *box2d.B2World) {
	if err := glfw.Init(); err != nil {
		log.Fatalln("failed to initialize glfw:", err)
		return
	}
	defer glfw.Terminate()

	glfw.WindowHint(glfw.Resizable, glfw.False)
	glfw.WindowHint(glfw.ContextVersionMajor, 4)
	glfw.WindowHint(glfw.ContextVersionMinor, 1)
	glfw.WindowHint(glfw.OpenGLProfile, glfw.OpenGLCoreProfile)
	glfw.WindowHint(glfw.OpenGLForwardCompatible, glfw.True)

	window, err := glfw.CreateWindow(windowWidth, windowHeight, "Box2D DebugDraw", nil, nil)
	if err != nil {
		panic(err)
	}
	window.MakeContextCurrent()

	// Initialize Glow
	if err := gl.Init(); err != nil {
		panic(err)
	}

	world.G_debugDraw.Create()
	gl.ClearColor(0.2, 0.2, 0.2, 1)
	// timeStep := 1.0 / 60.0
	// velocityIterations := 8
	// positionIterations := 3

	for !(window.ShouldClose() || closing) {
		gl.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)

		// Update
		// time := glfw.GetTime()
		// elapsed := time - previousTime
		// previousTime = time

		// angle += elapsed
		// model = mgl32.HomogRotate3D(float32(angle), mgl32.Vec3{0, 1, 0})

		// // Render
		// gl.UseProgram(program)
		// gl.UniformMatrix4fv(modelUniform, 1, false, &model[0])

		// gl.BindVertexArray(vao)

		// gl.ActiveTexture(gl.TEXTURE0)
		// gl.BindTexture(gl.TEXTURE_2D, texture)

		// gl.DrawArrays(gl.TRIANGLES, 0, 6*2*3)

		// world.Step(timeStep, velocityIterations, positionIterations)

		world.DrawDebugData()
		world.G_debugDraw.Flush()

		// Maintenance
		window.SwapBuffers()
		glfw.PollEvents()
	}
}
